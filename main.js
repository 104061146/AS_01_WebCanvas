// ----------------- element in html -----------------
var canvas = document.getElementById('sheet');
var pencil = document.getElementById('pencil');
var text = document.getElementById('text');
var font_menu = document.getElementById('font-menu');
var triangle = document.getElementById('triangle');
var rectangle = document.getElementById('rectangle');
var circle = document.getElementById('circle');
var line = document.getElementById('line');
var eraser = document.getElementById('eraser');
var reset = document.getElementById('reset');
var undo = document.getElementById('undo');
var redo = document.getElementById('redo');
var upload = document.getElementById('upload');
var size_p = document.getElementById('size_p');
var size_m = document.getElementById('size_m');
var set_font = document.getElementById('set_font');
var text_ctx = document.getElementById('text_ctx');
var ok_btn = document.getElementById('button-addon2');
var sel_size = document.getElementById('sel_size');
var sel_font = document.getElementById('sel_font');
var sel_w = document.getElementById('sel_w');
var button = document.getElementById('btn-download');
var rainbow = document.getElementById('rainbow')
// ---------------------------------------------------

// ----------------- global variable declaration -----------------
var prev_move = undefined;
var prev_down = undefined;
var prev_up = undefined;
var prev_keydown = undefined;

var done = new Array();
var redone = new Array();
var daction = undefined;

var mode = 0;
var mx1, my1, mx2, my2;
var clickX = new Array();
var clickY = new Array();
var clickDrag = new Array();
var lastX = undefined;
var lastY = undefined;
var firstX = 0;
var firstY = 0;
var paint;
var cur_color = '#ff0000';
var cur_text = '';
var font_type = "12px Arial";
var brush_size = 2;
var tmp_move = false;
var painted = false;
var rainbow_color = 0;

var rect = canvas.getBoundingClientRect();
var ctx = canvas.getContext('2d');
var func = [undefined, undefined, undefined];


class Action{
    constructor(fromX, fromY, toX, toY, color, width, type){
        this.type = type;
        this.fromX = fromX;
        this.fromY = fromY;
        this.toX = toX;
        this.toY = toY;
        this.color = color;
        this.width = width;
        redone = [];
    }

    recover(){
        if (this.type == 'pencil'){
            ctx.beginPath();
            ctx.moveTo(this.fromX, this.fromY);
            ctx.lineTo(this.toX, this.toY);
            ctx.strokeStyle = this.color;
            ctx.lineWidth = this.width;
            ctx.lineCap = "round"
            ctx.stroke();
            ctx.closePath();
        }
        else if (this.type == 'rainbow'){
            ctx.beginPath();
            ctx.moveTo(this.fromX, this.fromY);
            ctx.lineTo(this.toX, this.toY);
            ctx.strokeStyle = hsl(this.color);
            ctx.lineWidth = this.width;
            ctx.lineCap = "round"
            ctx.stroke();
            ctx.closePath();
        }
        else if (this.type == 'line'){
            ctx.beginPath();
            ctx.strokeStyle = this.color;
            ctx.lineWidth = this.width;
            ctx.moveTo(this.fromX, this.fromY);
            ctx.lineTo(this.toX, this.toY);
            ctx.stroke();
            ctx.closePath();
        }
        else if (this.type == 'rect'){
            ctx.beginPath();
            ctx.rect(this.fromX, this.fromY, this.toX, this.toY);
            ctx.fillStyle = this.color;
            ctx.fill();
            ctx.closePath();
        }
        else if (this.type == 'reset'){
            ctx.clearRect(this.fromX, this.fromY, this.toX, this.toY);
        }
        else if (this.type == 'triangle'){
            ctx.beginPath();
            ctx.moveTo(mx1,my1);
            ctx.lineTo(mx1,my2);
            ctx.lineTo(mx2,my2);
            ctx.closePath()
            ctx.fillStyle = this.color;
            ctx.fill();
        }
        else if (this.type == 'circle'){
            console.log('try to recover circle');
            ctx.beginPath();
            let r = Math.sqrt(Math.pow(this.toX-this.fromX, 2) + Math.pow(this.fromY-this.toY, 2));
            ctx.arc(this.fromX, this.fromY, r, 0, 2*Math.PI);
            ctx.fillStyle = this.color;
            ctx.fill();
            ctx.strokeStyle = this.color;
            ctx.stroke();
            ctx.closePath();
        }
        else if (this.type == 'text'){
            ctx.font = this.toY;
            ctx.fillStyle = this.color;
            ctx.fillText(this.toX, this.fromX, this.fromY);
        }
        else if (this.type == 'upload'){
            ctx.drawImage(this.color, 0, 0, canvas.width, canvas.height);
        }
    }
}

class DrawAction{
    constructor(fromX, fromY, toX, toY, color, width, type){
        this.actions = new Array();
    }
    recover(){
        for (let i=0; i<this.actions.length; i++){
            this.actions[i].recover();
        }
    }
}

function getMousePos(event) {
    return {
        x: (event.clientX - rect.left) * (canvas.width / rect.width),
        y: (event.clientY - rect.top) * (canvas.height / rect.height)
    }
}

function removeListener() {
    try {
        canvas.removeEventListener('mousedown', prev_down);
    }
    catch (e) {
        console.log(e);
    }
    try{
        canvas.removeEventListener('mouseup', prev_up);
    }
    catch(e){
        console.log(e)
    }
    try{
        canvas.removeEventListener('mousemove', prev_move);
    }
    catch(e){
        console.log(e)
    }
}

function usePencil_(){
    removeListener();
    canvas.style.cursor = 'url(img/cursor.cur), default';
    prev_move = function (event) {
        if(paint) {
            firstX = lastX;
            firstY = lastY;
            pos = getMousePos(event);
            lastX = pos.x;
            lastY = pos.y;
            if (firstX != undefined)
                drawing(cur_color, brush_size);
        }
    };
    prev_down = function (event) {
        paint=true;
        daction = new DrawAction();
    };
    prev_up = function (event) {
        lastX = undefined;
        lastY = undefined
        paint = false;
        done.push(daction);
        daction = undefined;
    };
    canvas.addEventListener('mousemove', prev_move)
    canvas.addEventListener('mousedown', prev_down);
    canvas.addEventListener('mouseup', prev_up);
}

function useRainbow(){
    removeListener();
    canvas.style.cursor = 'url(img/cursor.cur), default';
    prev_move = function (event) {
        if(paint) {
            firstX = lastX;
            firstY = lastY;
            pos = getMousePos(event);
            lastX = pos.x;
            lastY = pos.y;
            if (firstX != undefined){
                drawing(rainbow_color, brush_size, true);
                rainbow_color = (rainbow_color+1) % 360;
            }
        }
    };
    prev_down = function (event) {
        paint=true;
        daction = new DrawAction();
    };
    prev_up = function (event) {
        lastX = undefined;
        lastY = undefined
        paint = false;
        done.push(daction);
        daction = undefined;
    };
    canvas.addEventListener('mousemove', prev_move)
    canvas.addEventListener('mousedown', prev_down);
    canvas.addEventListener('mouseup', prev_up);
}

function hsl(color){
    return 'hsl('+color+', 100%, 50%)';
}

function drawing(color="#df4b26", b_size=2, is_rainbow=false){
    ctx.beginPath();
    ctx.moveTo(firstX, firstY);
    ctx.lineTo(lastX, lastY);
    if (is_rainbow){
        ctx.strokeStyle = hsl(color);
        console.log(hsl(color));
    }
    else
        ctx.strokeStyle = color;
    ctx.lineWidth = b_size;
    ctx.lineCap = "round"
    ctx.stroke();
    ctx.closePath();
    if (is_rainbow)
        daction.actions.push(new Action(firstX, firstY, lastX, lastY, 
        color, b_size, 'rainbow'));
    else
    daction.actions.push(new Action(firstX, firstY, lastX, lastY, 
        color, b_size, 'pencil'));
}

function useText() {
    canvas.style.cursor = 'text';
    console.log('switch to text input');
    removeListener();
    prev_up = function () {
        let pos = getMousePos(event)
        font_type = sel_w.selectedOptions[0].innerHTML+' '+sel_size.selectedOptions[0].innerHTML+' '+sel_font.selectedOptions[0].innerHTML;
        ctx.font = font_type;
        ctx.fillStyle = cur_color;
        cur_text = prompt('Please input the text:');
        ctx.fillText(cur_text, pos.x, pos.y);
        done.push(new Action(pos.x, pos.y, cur_text, font_type, cur_color, 1, 'text'));
    };
    canvas.addEventListener('mouseup', prev_up);
}
// function useText() {
//     canvas.style.cursor = 'text';
//     console.log('switch to text input');
//     removeListener();
//     let xx=undefined, yy=undefined, tt='';
//     prev_up = function () {
//         let pos = getMousePos(event)
//         font_type = sel_w.selectedOptions[0].innerHTML+' '+sel_size.selectedOptions[0].innerHTML+' '+sel_font.selectedOptions[0].innerHTML;
//         ctx.font = font_type;
//         ctx.fillStyle = cur_color;
//         xx = pos.x;
//         yy = pos.y;
//         tt = '';
//         console.log('click text');
//     };
//     prev_keydown = function(event){
//         console.log('current:', tt);
//         tt += event.key;
//         ctx.fillText(tt, xx, yy);
//         done.push(new Action(xx, yy, tt, font_type, cur_color, 1, 'text'));
//     };
//     canvas.addEventListener('click', prev_up);
//     canvas.addEventListener('keydown', prev_keydown);
// }

ok_btn.addEventListener('click', function(){
    cur_text = text_ctx.value;
});

function drawCircle() {
    canvas.style.cursor = 'url(img/circle.cur), default';
    console.log('switch to circle');
    removeListener();
    prev_down = function (event) {
        console.log('mousedown (circle)');
        let pos = getMousePos(event)
        mx1 = pos.x;
        my1 = pos.y;
        ctx.beginPath();
    };
    prev_up = function (event) {
        console.log('mouseup (circle)');
        let pos = getMousePos(event)
        let r = Math.sqrt(Math.pow(pos.x-mx1, 2) + Math.pow(pos.y-my1, 2));
        ctx.arc(mx1, my1, r, 0, 2*Math.PI);
        ctx.fillStyle = cur_color;
        ctx.fill();
        ctx.strokeStyle = cur_color;
        ctx.stroke();
        ctx.closePath();
        done.push(new Action(mx1, my1, pos.x, pos.y, cur_color, 1, 'circle'));
    };
    canvas.addEventListener('mousedown', prev_down);
    canvas.addEventListener('mouseup', prev_up);
}

function drawRectangle() {
    canvas.style.cursor = 'url(img/rect.cur), default';
    console.log('switch to rectangle');
    removeListener();
    prev_down = function (event) {
        console.log('mousedown');
        let pos = getMousePos(event)
        mx1 = pos.x;
        my1 = pos.y;
        ctx.beginPath();
    };
    prev_up = function (event) {
        console.log('mouseup');
        let pos = getMousePos(event)
        ctx.rect(mx1, my1, pos.x-mx1, pos.y-my1);
        ctx.fillStyle = cur_color;
        ctx.fill();
        ctx.closePath();
        console.log(mx1, my1, pos.x-mx1, pos.y-my1);
        done.push(new Action(mx1, my1,pos.x-mx1, pos.y-my1, cur_color, 1, 'rect'));
    };
    canvas.addEventListener('mousedown', prev_down);
    canvas.addEventListener('mouseup', prev_up);
}

function drawTriangle() {
    canvas.style.cursor = 'url(img/tri.cur), default';
    console.log('switch to triangle');
    removeListener();
    prev_down = function (event) {
        console.log('mousedown');
        let pos = getMousePos(event)
        mx1 = pos.x;
        my1 = pos.y;
        ctx.beginPath();
    };
    prev_up = function (event) {
        console.log('mouseup');
        let pos = getMousePos(event)
        mx2 = pos.x;
        my2 = pos.y;
        ctx.moveTo(mx1,my1);
        ctx.lineTo(mx1,my2);
        ctx.lineTo(mx2,my2);
        ctx.closePath()
        ctx.fillStyle = cur_color;
        ctx.fill();
        done.push(new Action(mx1, my1, mx2, my2, cur_color, 1, 'triangle'));
    };

    canvas.addEventListener('mousedown', prev_down);
    canvas.addEventListener('mouseup', prev_up);
}

function drawLine() {
    canvas.style.cursor = 'url(img/line.cur), default';
    console.log('switch to line');
    removeListener();
    prev_down = function (event) {
        console.log('mousedown (line)');
        let pos = getMousePos(event)
        firstX = pos.x;
        firstY = pos.y;
        ctx.beginPath();
        ctx.moveTo(firstX, firstY);
    };
    prev_up = function (event) {
        console.log('mouseup (line)');
        let pos = getMousePos(event)
        lastX = pos.x;
        lastY = pos.y;
        ctx.lineTo(lastX, lastY);
        ctx.lineWidth = brush_size;
        ctx.strokeStyle = cur_color;
        ctx.stroke();
        ctx.closePath();
        daction = new Action(firstX, firstY, lastX, lastY, cur_color, brush_size, 'line');
        lastX = undefined;
        lastY = undefined;
        done.push(daction);
    };
    canvas.addEventListener('mousedown', prev_down);
    canvas.addEventListener('mouseup', prev_up);
}

function useEraser(){
    canvas.style.cursor = 'url(img/eraser.cur), default';
    removeListener();
    prev_move = function (event) {
        if(paint) {
            firstX = lastX;
            firstY = lastY;
            pos = getMousePos(event);
            lastX = pos.x;
            lastY = pos.y;
            if (firstX != undefined)
                drawing("#ffffff", 5);
        }
    };
    prev_down = function (event) {
        daction = new DrawAction();
        paint=true;
    };
    prev_up = function (event) {
        lastX = undefined;
        lastY = undefined
        paint = false;
        done.push(daction);
        daction = undefined;
    };

    canvas.addEventListener('mousemove', prev_move)
    canvas.addEventListener('mousedown', prev_down);
    canvas.addEventListener('mouseup', prev_up);
}

function resetCanvas(warn=false, is_remove=true) {
    if (warn){
        if (confirm('Eraser all the content on the board ??')) {
            removeListener();
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            done.push(new Action(0, 0, canvas.width, canvas.height, '#ffffff', 0, 'reset'))
        }
        else {
            console.log('cancel reset');
        }
    }
    else{
        removeListener();
        ctx.clearRect(0, 0, canvas.width, canvas.height);
    }
}


// ------------- add event listener -------------
pencil.addEventListener('click', usePencil_);
text.addEventListener('click', useText);
rectangle.addEventListener('click', drawRectangle);
triangle.addEventListener('click', drawTriangle);
circle.addEventListener('click', drawCircle);
line.addEventListener('click', drawLine);
rainbow.addEventListener('click', useRainbow);

eraser.addEventListener('click', useEraser);
reset.addEventListener('click', resetCanvas);

font_menu.addEventListener('click', function(){
    $('.collapse').collapse('toggle');
    font_type = sel_w.selectedOptions[0].innerHTML+' '+sel_size.selectedOptions[0].innerHTML+' '+sel_font.selectedOptions[0].innerHTML;
});

function undo_fn(tmp=false, is_remove=true) {
    if (done.length > 0){
        console.log('undo');
        resetCanvas();
        redone.push(done.pop());
        for (let i=0; i<done.length; i++){
            done[i].recover();
        }
    }
    else{
        console.log('can\'t undo');
    }
};

function redo_fn(tmp=false, is_remove=true){
    if (redone.length>0){
        console.log('redo');
        resetCanvas();
        for (let i=0; i<done.length; i++){
            done[i].recover();
        }
        redone[redone.length-1].recover();
        done.push(redone.pop());
    }
    else{
        console.log('can\'t redo');
    }
}

undo.addEventListener('click', undo_fn);

redo.addEventListener('click', redo_fn);

size_p.addEventListener('click', function(){
    brush_size++;
});

size_m.addEventListener('click', function(){
    brush_size--;
})

upload.addEventListener('change', function (e){
    console.log(e);
    var reader = new FileReader();
    reader.onload = function(event){
        var img = new Image();
        img.onload = function(){
            ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
        }
        img.src = event.target.result;
        done.push(new Action(0, 0, canvas.width, canvas.height, img, 0, 'upload'));
    }
    reader.readAsDataURL(e.target.files[0]);     
});

button.addEventListener('click', function (e) {
    var dataURL = canvas.toDataURL('image/png');
    button.href = dataURL;
});

sel_font.addEventListener('change', function(e){
    font_type = sel_w.selectedOptions[0].innerHTML+' '+sel_size.selectedOptions[0].innerHTML+' '+sel_font.selectedOptions[0].innerHTML;
});
sel_size.addEventListener('change', function(e){
    font_type = sel_w.selectedOptions[0].innerHTML+' '+sel_size.selectedOptions[0].innerHTML+' '+sel_font.selectedOptions[0].innerHTML;
});
sel_w.addEventListener('change', function(e){
    font_type = sel_w.selectedOptions[0].innerHTML+' '+sel_size.selectedOptions[0].innerHTML+' '+sel_font.selectedOptions[0].innerHTML;
});

var color_block = document.getElementById('color-block');
var ctx_block = color_block.getContext('2d');
var width1 = color_block.width;
var height1 = color_block.height;

var color_strip = document.getElementById('color-strip');
var ctx_strip = color_strip.getContext('2d');
var width2 = color_strip.width;
var height2 = color_strip.height;

var colorLabel = document.getElementById('color-label');

var x = 0;
var y = 0;
var drag = false;
var rgba_color = 'rgba(255,0,0,1)';

ctx_block.rect(0, 0, width1, height1);
fillGradient();

ctx_strip.rect(0, 0, width2, height2);
var gardient = ctx_strip.createLinearGradient(0, 0, 0, height1);
gardient.addColorStop(0, 'rgba(255, 0, 0, 1)');
gardient.addColorStop(0.17, 'rgba(255, 255, 0, 1)');
gardient.addColorStop(0.34, 'rgba(0, 255, 0, 1)');
gardient.addColorStop(0.51, 'rgba(0, 255, 255, 1)');
gardient.addColorStop(0.68, 'rgba(0, 0, 255, 1)');
gardient.addColorStop(0.85, 'rgba(255, 0, 255, 1)');
gardient.addColorStop(1, 'rgba(255, 0, 0, 1)');
ctx_strip.fillStyle = gardient;
ctx_strip.fill();

function click(event) {
    x = event.offsetX;
    y = event.offsetY;
    var imageData = ctx_strip.getImageData(x, y, 1, 1).data;
    rgba_color = 'rgba(' + imageData[0] + ',' + imageData[1] + ',' + imageData[2] + ',1)';
    fillGradient();
}

function fillGradient() {
    ctx_block.fillStyle = rgba_color;
    ctx_block.fillRect(0, 0, width1, height1);

    var grd_white = ctx_strip.createLinearGradient(0, 0, width1, 0);
    grd_white.addColorStop(0, 'rgba(255,255,255,1)');
    grd_white.addColorStop(1, 'rgba(255,255,255,0)');
    ctx_block.fillStyle = grd_white;
    ctx_block.fillRect(0, 0, width1, height1);

    var grd_black = ctx_strip.createLinearGradient(0, 0, 0, height1);
    grd_black.addColorStop(0, 'rgba(0,0,0,0)');
    grd_black.addColorStop(1, 'rgba(0,0,0,1)');
    ctx_block.fillStyle = grd_black;
    ctx_block.fillRect(0, 0, width1, height1);
}

function mousedown(event) {
    drag = true;
    changeColor(event);
}

function mousemove(event) {
    if (drag) {
        changeColor(event);
    }
}

function mouseup(event) {
    drag = false;
}

function changeColor(event) {
    x = event.offsetX;
    y = event.offsetY;
    var imageData = ctx_block.getImageData(x, y, 1, 1).data;
    rgba_color = 'rgba(' + imageData[0] + ',' + imageData[1] + ',' + imageData[2] + ',1)';
    colorLabel.style.backgroundColor = rgba_color;
    cur_color = rgba_color;
}

color_strip.addEventListener("click", click, false);
color_block.addEventListener("mousedown", mousedown, false);
color_block.addEventListener("mouseup", mouseup, false);
color_block.addEventListener("mousemove", mousemove, false);

ctx.beginPath();
ctx.rect(0, 0, canvas.width, canvas.height);
ctx.fillStyle = '#ffffff';
ctx.fill();
ctx.closePath();