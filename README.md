# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here
> Please see demo video in this [[link]](https://www.youtube.com/watch?v=WGeIabeWdJE&feature=youtu.be).
- Pencil: We keep drawing lines as the mouse is moving and pressed. These are two buttons (size + and size -) used to change brush size.
- Eraser: Draw white on the canvas.
- Color Picker: We create a box for gradient of a color. Besides, we create another color bar of rainbow. To pick a color, please click on the gradient box. You will see the color label changes.
- Text: Click the place you want to input the text. Then, you can enter the text. Besids, we build a text menu with `<select>` and `<option>`. To change the font, please click font menu, and you see the selection menu of font family, font size and font weight
- Shapes: We get the mouse position when mousedown event or mouseup event happen. Then, we use these position to draw circle, triangle, rectangle or line. 
- Undo and Redo: For each action, we create an action object. Then, we push these action objects into an array (done array). If we want to undo an action, we pop an action from the array and put this object into another array (redone array). Then, we reset the canvas and redo all the action in done array. To redo an action, we recovery an action popped from the redone array.

- Reset: Clear the canvas with a rectangle.
- Upload: Click the upload button and select a image. Then the image will be pasted to the canvas.
- Download: Save the image your computer. We turn the canvas into a data url, and we use `<a>` to link the url.
- Cursor Icon: Cursor icon will change when we change the mode. To change the cursor icon, we change CSS style of cursor.

### Additional feature
- rainbow: utilize the HSL color domain and increment hue from 0 to 360.
